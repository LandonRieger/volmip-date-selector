from os import path
from setuptools import setup, find_packages
import versioneer

setup(name='volmip-date-selector',
      version=versioneer.get_version(),
      cmdclass=versioneer.get_cmdclass(),
      description='Select start dates for VolMIP simulations based on NAO and ENSO indices.',
      author='Landon Rieger',
      author_email='landon.rieger@usask.ca',
      license='MIT',
      url='https://gitlab.com/LandonRieger/volmip-date-selector.git',
      packages=find_packages(),
      package_data={'canesm_cmip6_aerosol': ['config_example.yaml']},
      install_requires=['netCDF4', 'dask', 'xarray>=0.13', 'cftime', 'click', 'matplotlib',
                        'nc-time-axis', 'numpy', 'pandas', 'appdirs', 'requests', 'esgf-pyclient', 'toolz'],
      entry_points='''
            [console_scripts]
            volmip-start-dates=volmip.scripts.cli:select_dates
            volmip-enso=volmip.scripts.cli:calculate_enso
            volmip-amoc=volmip.scripts.cli:calculate_amoc
            volmip-nao=volmip.scripts.cli:calculate_nao
            '''
      )
