import click
from volmip.scripts.cli import calculate_enso


if __name__ == '__main__':
    # calculate_enso(['CanESM5', 'r1i1p2f1', '--experiment-id=piControl'])
    calculate_enso(['CanESM5', 'r1i1p2f1', '--experiment-id=volc-pinatubo-full', '--parent-anomaly'])