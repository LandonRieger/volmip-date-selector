from volmip.util import extend_from_parent_experiment
from volmip.esgf import download_data


def test_concat():

    tos = download_data('CanESM5', 'volc-pinatubo-full', 'tos', 'Omon', 'r1i1p2f1', 'mon')
    tos = extend_from_parent_experiment(tos)


if __name__ == '__main__':
    test_concat()