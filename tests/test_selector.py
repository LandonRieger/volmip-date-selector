from volmip.date_selector import start_times, get_restart_times
import numpy as np
import pandas as pd
import xarray as xr


def test_start_times():

    np.random.seed(0)
    nao = xr.DataArray(np.random.normal(size=500), coords=[pd.date_range('1750', '2249', freq='AS')], dims=['time'])
    n34 = xr.DataArray(np.random.normal(size=500), coords=[pd.date_range('1750', '2249', freq='AS')], dims=['time'])
    restart = start_times(nao, n34, 1.5, 4)
    assert [pd.Timestamp(t).year for t in restart.time.values] == [1942, 1999, 2038, 2179]


def test_get_index():

    np.random.seed(0)
    nao = xr.DataArray(np.random.normal(size=500), coords=[pd.date_range('1750', '2249', freq='AS')], dims=['time'])
    n34 = xr.DataArray(np.random.normal(size=500), coords=[pd.date_range('1750', '2249', freq='AS')], dims=['time'])
    restart = get_restart_times(nao, n34, 4, 2)
    assert [pd.Timestamp(t).year for t in restart.time.values] == [1999, 2171, 2017, 2052]


if __name__ == '__main__':

    test_start_times()
    test_get_index()