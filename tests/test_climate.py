from volmip.esgf import download_data
from volmip.util import sample_season, extend_from_parent_experiment
from volmip.climate import compute_nino34, compute_nao, compute_amoc


def test_enso():

    data = download_data('CanESM5', 'volc-pinatubo-full', 'ts', 'Amon', 'r2i1p2f1', 'mon')
    data = extend_from_parent_experiment(data)
    nino = sample_season(compute_nino34(data.ts), 'djf')


def test_amoc():

    data = download_data('CanESM5', 'volc-pinatubo-full', 'ts', 'Amon', 'r20i1p2f1', 'mon')
    data = extend_from_parent_experiment(data)
    amoc = compute_amoc(data.msftmz)


def test_nao():

    data = download_data('CanESM5', 'volc-pinatubo-full', 'zg', 'Amon', 'r20i1p2f1', 'mon')
    data = extend_from_parent_experiment(data)
    nao = compute_nao(data.zg)


if __name__ == '__main__':
    test_amoc()
    test_enso()