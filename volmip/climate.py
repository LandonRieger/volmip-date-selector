import xarray as xr
from volmip.util import area_average, deseasonalize, remove_base
import cftime
import numpy as np


def convert_time(data: xr.DataArray):
    """
    Convert time field to cftime.DatetimeGregorian

    Parameters
    ----------
    data:
        Input xarray.DataArray

    Returns
    -------
        xr.DataArray
    """
    ctime = []
    for t in data.time.values:
        date = str(t).split(' ')[0].split('-')
        time = str(t).split(' ')[1].split(':')
        ctime.append(cftime.DatetimeGregorian(int(date[0]), int(date[1]), int(date[2]),
                                              int(time[0]), int(time[1]), int(time[2])))
    data['time'] = np.array(ctime)
    return data


def compute_nino34(ts: xr.DataArray, ts_anom: xr.DataArray = None) -> xr.DataArray:
    """
    Compute the ENSO 3.4 index from a CMIP6 model dataset

    Parameters
    ----------
    ts:
        Surface temperature
    ts_anom:
        Surface temperature used for the anomaly calculation. If empty then `ts` is used instead

    Returns
    -------
        ENSO 3.4 Index
    """

    # definition of mid and north-atlantic regions
    nino_lat = (-5, 5)
    nino_lon = (190, 240)

    # subset and average the data
    print('computing enso index')
    ts = area_average(ts, nino_lat, nino_lon)

    # depending on start/end dates some time fields can be in different formats, eg Timestamp vs cftime
    if not isinstance(type(ts.time.values[0]), type(ts.time.values[-1])):
        ts = convert_time(ts)

    if ts_anom is None:
        ts = deseasonalize(ts)
    else:
        ts_anom = area_average(ts_anom, nino_lat, nino_lon)
        ts = deseasonalize(ts, ts_anom)

    ts = remove_base(ts, 30)
    # compute rolling mean for standard deviation calculation
    ts_rolling = ts.rolling(time=3, center=True).mean(dim='time')
    ts = ts.groupby('time.month') / ts_rolling.groupby('time.month').std(dim='time')
    return ts.rename('nino34')


def compute_nao(zg: xr.DataArray, zg_anom: xr.DataArray = None) -> xr.DataArray:
    """
    Compute the North Atlantic Oscillation from a CMIP6 model dataset

    Parameters
    ----------
    zg:
        Geopotential height of pressure levels
    zg_anom:
        Geopotential height used for the anomaly calculation. If empty then `zg` is used instead

    Returns
    -------
        North Atlantic Oscillation
    """
    # pressure level to compute NAO
    plev = 50000  # [Pa]

    # definition of mid and north-atlantic regions
    ma_lat = (20, 55)
    ma_lon = (-90, 60)
    na_lat = (55, 90)
    na_lon = (-90, 60)

    # subset and average the data
    print('computing nao index')
    zg = zg.sel(plev=plev, method='nearest')

    if not isinstance(type(zg.time.values[0]), type(zg.time.values[-1])):
        zg = convert_time(zg)

    ma_t = area_average(zg, ma_lat, ma_lon)
    na_t = area_average(zg, na_lat, na_lon)

    if zg_anom is not None:
        zg_anom = zg_anom.sel(plev=plev, method='nearest')
        ma_t_anom = area_average(zg_anom, ma_lat, ma_lon)
        na_t_anom = area_average(zg_anom, na_lat, na_lon)
        ma_t = deseasonalize(ma_t, ma_t_anom).groupby('time.month') / ma_t_anom.groupby('time.month').std(dim='time')
        na_t = deseasonalize(na_t, na_t_anom).groupby('time.month') / na_t_anom.groupby('time.month').std(dim='time')
        ma_t_anom_d = deseasonalize(ma_t_anom).groupby('time.month') / ma_t_anom.groupby('time.month').std(dim='time')
        na_t_anom_d = deseasonalize(na_t_anom).groupby('time.month') / na_t_anom.groupby('time.month').std(dim='time')
        nao_anom = (ma_t_anom_d - na_t_anom_d).rename('nao')
    else:
        ma_t = deseasonalize(ma_t).groupby('time.month') / ma_t.groupby('time.month').std(dim='time')
        na_t = deseasonalize(na_t).groupby('time.month') / na_t.groupby('time.month').std(dim='time')

    nao = (ma_t - na_t).rename('nao')
    if zg_anom is not None:
        nao = nao.groupby('time.month') / nao_anom.groupby('time.month').std(dim='time')
    else:
        nao = nao.groupby('time.month') / nao.groupby('time.month').std(dim='time')

    return nao


def compute_amoc(msftmz: xr.DataArray, msftmz_anom: xr.DataArray = None) -> xr.DataArray:
    """

    Parameters
    ----------
    msftmz :
        Ocean meridional overturning mass streamfunction
    msftmz_anom:
        Dataset used for the anomaly calculation. If empty then `msftmz` is used instead


    Returns
    -------
        AMOC
    """
    print('computing amoc index')
    amoc = msftmz.sel(lat=slice(20, 60)).sel(basin=0).max(dim=['lev', 'lat']).load().rename('amoc')

    if not isinstance(type(amoc.time.values[0]), type(amoc.time.values[-1])):
        amoc = convert_time(amoc)

    if msftmz_anom is not None:
        amoc_anom = msftmz_anom.sel(lat=slice(20, 60)).sel(basin=0).max(dim=['lev', 'lat']).load().rename('amoc')
        amoc = deseasonalize(amoc, amoc_anom).groupby('time.month') / amoc_anom.groupby('time.month').std(dim='time')
    else:
        amoc = deseasonalize(amoc).groupby('time.month') / amoc.groupby('time.month').std(dim='time')

    return amoc.rename('amoc')
