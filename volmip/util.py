import xarray as xr
from typing import Tuple
import numpy as np
from volmip.esgf import download_data
import cftime


def area_average(data: xr.DataArray, lat_bin: Tuple[float, float], lon_bin: Tuple[float, float]) -> xr.DataArray:
    """
    Deseasonalize and compute the area weighted average.
    Average the result seasonally and return only DJF

    Parameters
    ----------
    data :
        data that will be averaged over the lat/lon bin
    lat_bin:
        latitudinal region of interest
    lon_bin:
        longitudinal region of interest

    Returns
    -------
        Data averaged and deseasonalized in the region of interest.
    """

    min_lat, max_lat = lat_bin
    min_lon, max_lon = lon_bin

    if (min_lon > 0) & (max_lon < 0):
        max_lon = 360 + max_lon

    if min_lon < 0:  # shift longitudes to -180 to 180
        data = data.assign_coords(lon=(((data.lon + 180) % 360) - 180)).sortby('lon')

    # select the bin we are interested in and average over longitude
    data = data.sel(lat=slice(min_lat, max_lat)) \
               .sel(lon=slice(min_lon, max_lon)) \
               .mean(dim='lon').load()

    # do the latitude weighted average
    data = (np.cos(data.lat * np.pi / 180) * data).sum(dim='lat') / \
           np.cos(data.lat * np.pi / 180).sum(dim='lat')

    return data


def deseasonalize(data: xr.DataArray, data_anom: xr.DataArray = None) -> xr.DataArray:
    """
    Deseasonalize the data, subtracting the climatological monthly data

    Parameters
    ----------
    data:
        data to be deseasonalized
    data_anom:
        data used for the anomaly calculation. If `None` then data is used.

    Returns
    -------
        deseasonalized data array
    """

    if data_anom is None:
        return data.groupby('time.month') - data.groupby('time.month').mean(dim='time')
    else:
        return data.groupby('time.month') - data_anom.groupby('time.month').mean(dim='time')


def sample_season(data: xr.DataArray, season: str = 'all') -> xr.DataArray:
    """
    Bin the data into seasons and return the season of interest

    Parameters
    ----------
    data :
        Data array
    season :
        Name of the season to be returned. Defaults to all

    Returns
    -------

    """

    data = data.resample(time='QS-DEC').mean(dim='time')
    season = season.lower()

    if season == 'djf':
        month = 12
    elif season == 'mam':
        month = 3
    elif season == 'jja':
        month = 6
    elif season == 'son':
        month = 9
    elif season == 'all':
        pass
    else:
        raise ValueError('unknown season')

    if season != 'all':
        data = data.where(data['time.month'] == month, drop=True)

    return data


def remove_base(data: xr.DataArray, base_period: int = 30, base_rotation: int = 5) -> xr.DataArray:
    """
    Remove the mean of the data in every `base_period`. This is typically done for indices like ENSO.

    Parameters
    ----------
    data :
        timeseries that will be normalized
    base_period :
        length of the base periods
    base_rotation :


    Returns
    -------
        normalized data
    """

    start_year = int(str(data.time.values[0]).split('-')[0])
    end_year = int(str(data.time.values[-1]).split('-')[0])
    base_years = np.arange(int(start_year / base_rotation) * base_rotation, end_year, base_rotation)

    half_base = int(base_period / 2)
    norm_data = []
    for year in base_years:
        if year - half_base < start_year:
            base_start_year = start_year
            base_end_year = start_year + base_period
            if base_end_year > end_year:
                base_end_year = end_year
                print(f'WARNING: insufficient data for full base period '
                      f'calculation during years {year}-{year + base_rotation -1}. '
                      f'Setting base period to {base_start_year}-{base_end_year}')
        elif year + half_base > end_year:
            base_start_year = end_year - base_period
            base_end_year = end_year
        else:
            base_start_year = year - half_base
            base_end_year = year + half_base
        temp = data.sel(time=slice(f'{year}-01-01', f'{year + base_rotation - 1}-12-31'))
        base = data.sel(time=slice(f'{base_start_year}-01-01', f'{base_end_year}-12-31'))
        norm_data.append(temp - base.mean(dim='time'))

    return xr.concat(norm_data, dim='time').sortby('time')


def extend_from_parent_experiment(data: xr.Dataset, cmip_folder: str = None, max_years: int = None,
                                  parent_experiment: str = None, parent_variant: str = None, parent_model: str = None,
                                  parent_branch_time: str = None, child_branch_time: str = None):
    """
    Extend an expiriment backward in time using its parent experiment. By default this will use the dataset attributes
    to do the extension.
    """

    if parent_experiment is None:
        parent_experiment = data.attrs['parent_experiment_id']

    if parent_variant is None:
        parent_variant = data.attrs['parent_variant_label']

    if parent_model is None:
        parent_model = data.attrs['parent_source_id']

    if parent_branch_time is None:
        parent_branch_time = convert_ymdh_to_cftime(data.attrs['YMDH_branch_time_in_parent'])

    if child_branch_time is None:
        child_branch_time = convert_ymdh_to_cftime(data.attrs['YMDH_branch_time_in_child'])

    parent_data = get_parent_experiment(data, cmip_folder, parent_experiment, parent_variant, parent_model)

    # TODO: does this and dropping overlap work if the month uses the first or last day instead of center?
    parent_data = parent_data.sel(time=slice(parent_data.time[0].values, parent_branch_time))
    if max_years is not None:
        start_year = int(str(parent_branch_time).split('-')[0]) - max_years
        parent_data = parent_data.sel(time=slice(f'{start_year}-01-01', str(parent_branch_time)))

    # shift the parent data onto the time coordinates of the child run
    time_shift = parent_branch_time - child_branch_time
    parent_data = parent_data.assign_coords(time=parent_data.time.values - time_shift)

    # drop the last parent point if it overlaps
    if parent_data.time[-1] == data.time[0]:
        parent_data = parent_data.isel(time=slice(0, -1))

    year_ext = int(str(parent_data.time[-1].values).split('-')[0]) - int(str(parent_data.time[0].values).split('-')[0])
    print(f'extending {data.variable_id} dataset by {year_ext} years '
          f'using {parent_experiment} {parent_model} {parent_variant}')
    return xr.concat([data, parent_data], dim='time').sortby('time')


def get_parent_experiment(data: xr.Dataset, cmip_folder: str = None,
                          parent_experiment: str = None, parent_variant: str = None, parent_model: str = None):
    """
    Extend an expiriment backward in time using its parent experiment. By default this will use the dataset attributes
    to do the extension.
    """

    if parent_experiment is None:
        parent_experiment = data.attrs['parent_experiment_id']

    if parent_variant is None:
        parent_variant = data.attrs['parent_variant_label']

    if parent_model is None:
        parent_model = data.attrs['parent_source_id']

    parent_data = download_data(model=parent_model, experiment_id=parent_experiment, variable=data.variable_id,
                                table_id=data.table_id, variant_label=parent_variant,
                                frequency=data.frequency, folder=cmip_folder).sortby('time')
    return parent_data


def convert_ymdh_to_str(ymdh):
    """
    convert date of YEAR:MONTH:DAY:HOUR format into ISO-8601 string
    """
    d = ymdh.split(':')
    return f'{d[0]}-{d[1]}-{d[2]} {d[3]}:00:00'


def convert_ymdh_to_cftime(ymdh):
    """
    convert date of YEAR:MONTH:DAY:HOUR format into `cftime.DateTimeNoLeap`
    """
    d = [int(s) for s in ymdh.split(':')]
    return cftime.DatetimeNoLeap(*d)
