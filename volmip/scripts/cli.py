from volmip.date_selector import find_start_years
from volmip.climate import compute_nao, compute_nino34, compute_amoc
from volmip.util import extend_from_parent_experiment, get_parent_experiment
from volmip.esgf import download_data, clear_downloaded_cache
import os
import click


@click.command()
@click.argument('model')
@click.argument('variant')
@click.option('-o', '--output-folder', default=None)
@click.option('-n', '--ensemble-size', default=25)
@click.option('-nn', '--num-neutral', default=4)
@click.option('-i', '--cmip-folder', default=None)
@click.option('-e', '--experiment-id', default='piControl')
@click.option('--amoc', is_flag=True)
@click.option('--clear-cache', is_flag=True)
def select_dates(model, variant, ensemble_size, num_neutral, experiment_id, output_folder, cmip_folder, clear_cache, amoc):

    if clear_cache:
        clear_downloaded_cache()
    else:
        find_start_years(model, variant,
                         ensemble_size=ensemble_size,
                         num_neutral=num_neutral,
                         experiment_id=experiment_id,
                         output_folder=output_folder,
                         cmip_data_folder=cmip_folder,
                         make_amoc=amoc)


@click.command()
@click.argument('model')
@click.argument('variant')
@click.option('--extend', is_flag=True, default=False)
@click.option('--max-extension', default=None)
@click.option('-o', '--output-folder', default=None)
@click.option('-i', '--cmip-folder', default=None)
@click.option('-e', '--experiment-id', default='piControl')
@click.option('-a', '--parent-anomaly', is_flag=True, default=False)
def calculate_amoc(model, variant, experiment_id, extend, max_extension, output_folder, cmip_folder, parent_anomaly):

    calculate_index('amoc', model, variant, experiment_id, extend,
                    max_extension, output_folder, cmip_folder, parent_anomaly)


@click.command()
@click.argument('model')
@click.argument('variant')
@click.option('--extend', is_flag=True, default=False)
@click.option('--max-extension', default=None)
@click.option('-o', '--output-folder', default=None)
@click.option('-i', '--cmip-folder', default=None)
@click.option('-e', '--experiment-id', default='piControl')
@click.option('-a', '--parent-anomaly', is_flag=True, default=False)
def calculate_enso(model, variant, experiment_id, extend, max_extension, output_folder, cmip_folder, parent_anomaly):

    calculate_index('nino34', model, variant, experiment_id, extend,
                    max_extension, output_folder, cmip_folder, parent_anomaly)


@click.command()
@click.argument('model')
@click.argument('variant')
@click.option('--extend', is_flag=True, default=False)
@click.option('--max-extension', default=None)
@click.option('-o', '--output-folder', default=None)
@click.option('-i', '--cmip-folder', default=None)
@click.option('-e', '--experiment-id', default='piControl')
@click.option('-a', '--parent-anomaly', is_flag=True, default=False)
def calculate_nao(model, variant, experiment_id, extend, max_extension, output_folder, cmip_folder, parent_anomaly):

    calculate_index('nao', model, variant, experiment_id, extend,
                    max_extension, output_folder, cmip_folder, parent_anomaly)


def get_year_str(ds):

    first_year = int(str(ds.time[0].values).split('-')[0])
    first_month = int(str(ds.time[0].values).split('-')[1])
    last_year = int(str(ds.time[-1].values).split('-')[0])
    last_month = int(str(ds.time[-1].values).split('-')[1])
    return f'{first_year:04d}{first_month:02d}-{last_year:04d}{last_month:02d}'


def calculate_index(index, model, variant, experiment_id, extend, max_extension, output_folder, cmip_folder, parent_anomaly):

    if output_folder is None:
        output_folder = 'output'
    os.makedirs(output_folder, exist_ok=True)

    if index == 'nino34':
        variable = 'ts'
        realm = 'Amon'
        # variable = 'tos'
        # realm = 'Omon'
    elif index == 'nao':
        variable = 'zg'
        realm = 'Amon'
    elif index == 'amoc':
        variable = 'msftmz'
        realm = 'Omon'
    else:
        raise ValueError(f'index: {index} is not recognized, choose from "nao", "nino34" or "amoc"')

    data = download_data(model, experiment_id, variable, realm, variant, 'mon', folder=cmip_folder)
    if parent_anomaly:
        parent_data = get_parent_experiment(data)
    else:
        parent_data = None

    if extend:
        if max_extension is not None:
            max_extension = int(max_extension)
        data = extend_from_parent_experiment(data, max_years=max_extension, cmip_folder=cmip_folder)

    if index == 'nino34':
        if parent_data is not None:
            parent_data = parent_data.ts
        result = compute_nino34(data.ts, parent_data).rolling(time=3, center=True).mean()
    elif index == 'nao':
        if parent_data is not None:
            parent_data = parent_data.zg
        result = compute_nao(data.zg, parent_data).rolling(time=3, center=True).mean()
    elif index == 'amoc':
        if parent_data is not None:
            parent_data = parent_data.msftmz
        result = compute_amoc(data.msftmz, parent_data).resample(time='AS').mean(dim='time')

    output_file = f'{index}_mon_{model}_{experiment_id}_{variant}_gr_{get_year_str(result)}.nc'
    output_file = os.path.abspath(os.path.join(output_folder, output_file))
    print(f"saving output to {output_file}")
    result.to_dataset(name=index).to_netcdf(os.path.join(output_folder, output_file))
    return result
