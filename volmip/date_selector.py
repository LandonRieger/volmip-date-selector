import os
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import cftime
from volmip.esgf import download_data
from volmip.climate import compute_nao, compute_nino34, compute_amoc
from volmip.util import sample_season


def find_start_years(model: str,
                     variant: str,
                     ensemble_size: int = 25,
                     num_neutral: int = 4,
                     experiment_id: str = 'piControl',
                     output_folder: str = None,
                     cmip_data_folder: str = None,
                     save_txt: bool = True,
                     make_plots: bool = True,
                     make_amoc: bool = False):

    if ensemble_size - num_neutral < 0:
        raise ValueError('ensemble size should be larger than number of neutral samples')

    if output_folder is None:
        output_folder = 'output'

    os.makedirs(output_folder, exist_ok=True)
    nc_folder = os.path.join(output_folder, 'netcdf')
    os.makedirs(nc_folder, exist_ok=True)

    # compute the nao and enso indices
    ts = download_data(model, experiment_id, 'ts', 'Amon', variant, 'mon', folder=cmip_data_folder)
    zg = download_data(model, experiment_id, 'zg', 'Amon', variant, 'mon', folder=cmip_data_folder)
    n34 = sample_season(compute_nino34(ts.ts), 'djf')
    nao = sample_season(compute_nao(zg.zg), 'djf')

    # save na and enso to output folder
    first_year = nao.time.values[0].year
    last_year = nao.time.values[-1].year
    nao.to_netcdf(os.path.join(nc_folder,
                               f'nao_DJF_{model}_{experiment_id}_{variant}_gr_{first_year}-{last_year}.nc'))
    n34.to_netcdf(os.path.join(nc_folder,
                               f'nino34_DJF_{model}_{experiment_id}_{variant}_gr_{first_year}-{last_year}.nc'))

    # find the start dates used for volmip
    restarts = get_restart_times(nao, n34, ensemble_size, num_neutral)
    nao_time = restarts.time
    restarts.assign_coords(time=[cftime.DatetimeProlepticGregorian(y, 1, 1) for y in restarts.start_year])
    restarts.to_netcdf(os.path.join(nc_folder,
                                    f'restarts_{model}_{experiment_id}_{variant}_gr_{first_year}-{last_year}.nc'))

    if save_txt:
        outDate = f'start-dates.{ensemble_size}-members.DJF_{experiment_id}_neut.txt'
        outNAOsel = f'NAO-selected.v2_DJF_{experiment_id}_neut.txt'
        outN34sel = f'N34-selected.v2_DJF_{experiment_id}_neut.txt'
        outDatesel = f'DATE-selected-VOLMIP-{experiment_id}_gr_{first_year}-{last_year}_neut.txt'

        out_folder = os.path.join(output_folder, 'txt')
        os.makedirs(out_folder, exist_ok=True)
        np.savetxt(os.path.join(out_folder, outNAOsel), restarts.nao.values, fmt='%f3', delimiter=',')
        np.savetxt(os.path.join(out_folder, outN34sel), restarts.n34.values, fmt='%f3', delimiter=',')

        # find a second set to extend the ensemble if needed
        restarts_extra = get_restart_times(nao.where(~nao.time.isin(nao_time), drop=True),
                                           n34.where(~n34.time.isin(nao_time), drop=True),
                                           ensemble_size, num_neutral)

        fout = open(os.path.join(out_folder, outDate), 'w')
        for y1, y2 in zip(restarts.start_year.values, restarts_extra.start_year.values):
            fout.write(f'{y1},{y2}\n')
        fout.close()

        fout = open(os.path.join(out_folder, outDatesel), 'w')
        for date in restarts.volc_pin.values:
            fout.write(f'{experiment_id}_{variant} {date.strftime("%Y-%m-%d")}\n')
        fout.close()

    if make_plots:
        os.makedirs(os.path.join(output_folder, 'figures'), exist_ok=True)
        plot(nao, n34, restarts, os.path.join(output_folder, 'figures'))

    if make_amoc:
        msftmz = download_data(model, experiment_id, 'msftmz', 'Omon', variant, 'mon', folder=cmip_data_folder)
        amoc = sample_season(compute_amoc(msftmz.msftmz), 'djf')
        amoc.to_netcdf(os.path.join(nc_folder,
                                    f'amoc_DJF_{model}_{experiment_id}_{variant}_gr_{first_year}-{last_year}.nc'))
        if make_plots:
            plot_amoc(amoc, restarts, os.path.join(output_folder, 'figures'))


def get_restart_times(nao: xr.DataArray, n34: xr.DataArray, ensemble_size: int, num_neutral: int = 0) -> xr.Dataset:
    """
    Select NAO/Nino34 indexes covering a maximum set of NAO and
    Nino34 phases combinaison (or neutral phases if specify).

    Based on algorithm by Myriam Kohdri.

    Parameters
    ----------
    nao :
        NAO values
    n34 :
        Nino34 values
    ensemble_size :
        Ensemble size
    num_neutral :
        Only select neutral index, by default 'False'

    Returns
    -------
    inao
        NAO indexes
    in34
        Nino34 index

    """

    # normalize and center
    nao_norm = (nao - nao.mean()) / nao.std()
    n34_norm = (n34 - n34.mean()) / n34.std()

    # find the non-neutral start dates
    start_time = start_times(nao_norm, n34_norm, radius=1.5, num_sections=ensemble_size - num_neutral)

    if num_neutral > 0:
        # remove the points we've already selected
        nao_norm = nao_norm.where(~nao_norm.time.isin(start_time), drop=True)
        n34_norm = n34_norm.where(~n34_norm.time.isin(start_time), drop=True)

        # find the neutral points and append them
        start_time_neutral = start_times(nao_norm, n34_norm, radius=0.05, num_sections=num_neutral)
        start_time = xr.concat([start_time, start_time_neutral], dim='time')

    # since DJF starts on the previous year add one
    start_years = [t.year + 1 for t in start_time.time.values]
    restarts = xr.Dataset({'start_year': (['time'], start_years),
                           'nao': (['time'], nao.where(nao.time.isin(start_time.time), drop=True)),
                           'n34': (['time'], n34.where(n34.time.isin(start_time.time), drop=True)),
                           'volc_pin': (['time'], [cftime.DatetimeProlepticGregorian(y, 5, 31)
                                                   for y in start_years]),
                           'volc_long': (['time'], [cftime.DatetimeProlepticGregorian(y, 4, 30)
                                                    for y in start_years])},
                          coords={'time': start_time.time}).sortby('time')

    return restarts


def start_times(nao: xr.DataArray, n34: xr.DataArray, radius: float, num_sections: int) -> xr.DataArray:
    """
    Partion a circle in NAO v. ENSO space into num_sections and return the start time from each section
    that is closest to the circle
    """

    start_time = min_distance_times(nao, n34, radius, num_sections)

    # if some sections were empty resample and append to get the correct number of ensembles
    while len(start_time) < num_sections:
        nao = nao.where(~nao.time.isin(start_time), drop=True)
        n34 = n34.where(~n34.time.isin(start_time), drop=True)
        if len(nao) == 0:
            raise ValueError('Number of ensembles is larger than NAO dataset')
        start_time_add = min_distance_times(nao, n34, radius, num_sections - len(start_time))
        start_time = xr.concat([start_time, start_time_add], dim='time')

    return start_time


def min_distance_times(nao: xr.DataArray, n34: xr.DataArray, radius: float, num_sections: int) -> xr.DataArray:
    """
    get the times of elements closest to circle
    """

    # compute the distance of the points to a circle of radius r
    cdist = np.abs(np.sqrt(nao ** 2 + n34 ** 2) - radius).rename('distance')

    # compute the phase of the points
    phi = np.arctan2(nao, n34).rename('angle')

    # select the point closest to the circle in each anglular section
    min_dist_per_section = cdist.groupby_bins(phi, bins=np.linspace(-np.pi, np.pi, num_sections + 1)).min()
    min_dist_per_section = min_dist_per_section.where(np.isfinite(min_dist_per_section), drop=True)
    return cdist.where(cdist.isin(min_dist_per_section), drop=True).time


def plot(nao, n34, restarts, output_folder):
    print('Generating plots')

    plt.figure(1)
    plt.subplot(211)
    nao.plot()
    plt.axhline(0, ls='--', lw=0.5, c='grey')
    plt.title('NAO')
    plt.xlabel('')

    plt.subplot(212)
    n34.plot()
    plt.axhline(0, ls='--', lw=0.5, c='grey')
    plt.title('Nino34')
    plt.xlabel('')
    plt.savefig(os.path.join(output_folder, 'nao_nino34_timeseries.pdf'))

    plt.figure(2)
    plt.scatter(nao, n34, s=40, edgecolors=plt.cm.tab10(0), label='all')
    plt.scatter(restarts.nao, restarts.n34, s=70, facecolors='none', edgecolors=plt.cm.tab10(1), label='selected')
    plt.axhline(0, ls='--', lw=0.5, c='grey')
    plt.axvline(0, ls='--', lw=0.5, c='grey')
    plt.xlabel('NAO index DJF')
    plt.ylabel('Nino34 index DJF')
    # plt.xlim([-3, 3])
    # plt.ylim([-3, 3])
    plt.legend()
    plt.savefig(os.path.join(output_folder, 'nao_nino34_selection.pdf'))


def plot_amoc(amoc, restarts, output_folder):

    n, bins = np.histogram(amoc.values, bins=20, density=True)
    nr = np.histogram(amoc.where(amoc.time.isin(restarts.time)).values, bins=bins, density=True)[0]

    x = bins[0:-1] + np.diff(bins)
    plt.figure()
    plt.bar(x, n, width=np.diff(bins)[0], label='All states')
    plt.bar(x, nr, width=np.diff(bins)[0], alpha=0.5, label='Sampled states')
    plt.xlabel('AMOC DJF')
    plt.ylabel('PDF')
    plt.legend()
    plt.savefig(os.path.join(output_folder, 'amoc_sampling.pdf'))


if __name__ == '__main__':

    find_start_years('CanESM5', 'r1i1p2f1', ensemble_size=25, num_neutral=4, experiment_id='piControl', make_amoc=True)
