import xarray as xr
import os
from pyesgf.search import SearchConnection
import requests
import appdirs
from urllib.request import urlretrieve
import shutil
import urllib


def download_data(model, experiment_id, variable, table_id,
                  variant_label: str = 'r1i1p1f1',
                  frequency: str = 'mon',
                  folder: str = None,
                  search_node: str = 'https://esgf-node.llnl.gov/esg-search') -> xr.Dataset:
    """
    Download a variable from the esgf server and place it into the cmip6 output folder structure

    Parameters
    ----------
    model :
        name of model
    experiment_id :
        name of cmip6 experiment
    variable :
        CMOR name of variable
    table_id :
        Table name, eg Amon
    variant_label :
        ensemble realization eg 'r1i1p1f1'
    frequency :
        time frequency, eg 'mon'
    folder :
        output folder where the data will be stored. Defaults to the python package cache directory

    Returns
    -------
        Dataset containing variable
    """

    print(f'getting {variable} for {model}-{experiment_id}-{variant_label}')
    conn = SearchConnection(search_node, distrib=True)

    max_attemp = 3
    for attemp in range(max_attemp):
        try:
            ctx = conn.new_context(
                project='CMIP6',
                source_id=model,
                experiment_id=experiment_id,
                variable=variable,
                frequency=frequency,
                table_id=table_id,
                variant_label=variant_label,
                facets='null')

            results = ctx.search()
        except requests.HTTPError as err:
            print('Retrying due to: ', err)
            attemp += 1
        else:
            break

    if folder is None:
        folder = data_folder()
    folder_struct = results[0].dataset_id.split('|')[0].split('.')
    folder = os.path.join(folder, *folder_struct)
    os.makedirs(folder, exist_ok=True)

    urls = []
    for result in results:
        for f in result.file_context().search():
            urls.append(f.download_url)

    output_files = []
    for url in urls:
        file = os.path.basename(url)
        newfile = os.path.join(folder, file)
        output_files.append(newfile)
        if not os.path.isfile(newfile):
            print(f'\tdownloading {file} to {folder}')
            try:
                r = urlretrieve(url, newfile)
            except urllib.error.HTTPError as err:
                print(f'error downloading {file}')

    return xr.open_mfdataset(list(set(output_files)), combine='by_coords')


def data_folder():

    return appdirs.user_cache_dir('volmip')


def clear_downloaded_cache():

    shutil.rmtree(data_folder(), ignore_errors=True)


if __name__ == '__main__':

    data = download_data('E3SM-1-0', 'piControl', 'ts', 'Amon', 'r1i1p1f1')
    # data = download_data('CanESM5', 'piControl', 'ts', 'Amon', 'r1i1p2f1')
    print(data)