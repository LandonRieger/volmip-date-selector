# volmip-date-selector

Select start dates for VolMIP simulations based on NAO and ENSO index.

## Installation

```bash
pip install git+ssh://git@gitlab.com/LandonRieger/volmip-date-selector.git
```

## Basic Use

### Calculating start dates
To determine the restart dates to use for your model run the command:
```bash
volmip-start-dates model_name variant_label
```

For example, for the `p2` variant of `CanESM5` model
```bash
volmip-start-dates CanESM5 r1i1p2f1
```

This will create the file 

```bash
restarts_{model}_piControl_{variant}_gr_{first_year}-{last_year}.nc
```

that contains the restart years, nao and enso values, as well 
as some additional plots and information. 

If the `ta` variable needs
to be downloaded this can take an hour or two to run, otherwise it should take a
minute or so.
If the data is already available in the recommended CMIP directory structure (https://www.earthsystemcog.org/site_media/projects/wip/CMIP6_global_attributes_filenames_CVs_v6.2.6.pdf)
then the folder can specified as

```bash
volmip-start-dates CanESM5 r1i1p2f1 -i path/to/cmip/data
```

where `path/to/cmip/data` is the folder just above the `CMIP6` folder.

### Command line options

To control the output a few options are available

| Option                  | Description | Default |
|:----------------------- |:--------  | ---------- |
| `-o/--output-folder`    | Location where output files will be placed   | cwd/output |
| `-n/--ensemble-size`    | Number of restart dates to generate | 25 |
| `-nn/--num-neutral`     | Number of restarts dates with NAO and ENSO values near zero | 4 |
| `-i/--cmip-folder`      | If piControl data is already available you can provide the CMIP folder to avoid downloading it | python cache directory |
| `-e/--experiment-id`    | Name of experiment | piControl |
| `--amoc`                | Compute and plot the AMOC values | Do not compute |
| `--clear-cache`         | Remove all downloaded data in the python cache without running the script | Leave data |

## Calculating Climate Indices

To compute volmip climate indices from ESGF data the following command line interface is available. 
If data is not found in the path it will be automatically downloaded. Since the volmip runs are often quite short, the
flag `--extend` can be used. This will concatenate the volmip runs with their parent run to improve calculation of the
base period. The option `--max-extension=30` can also be used to set the maximum length of the extension. Another option 
is to use the full parent run for the anomaly calculations. This is done using  the `--parent-anomaly` flag. 
See [doi:10.5194/gmd-9-2701-2016](https://gmd.copernicus.org/articles/9/2701/2016/gmd-9-2701-2016.pdf) 
for calculation details.

### Enso
```bash
volmip-enso CanESM5 r1i1p2f1 --experiment-id=volc-pinatubo-full --parent-anomaly --cmip-folder path/to/cmip/data
```
Computed as the area-averaged, deseasonalized sea surface temperature normalized by the standard deviation of the 
3-month rolling average. A moving base period of 30 years is used to avoid incorporating trends. 
Period is shifted every 5 years.

- Latitude range: 5S to 5N
- Longitude range: 170W to 120W

### NAO
```bash
volmip-nao CanESM5 r1i1p2f1 --experiment-id=volc-pinatubo-full --extend --cmip-folder path/to/cmip/data
```
- Latitude range: 20N to 55N and 55N to 90N
- Longitude range: 90W to 60E

Computed from the difference in 500 hPa level anomalies between the north and mid-atlantic regions. 
Anomalies are computed as:

```math
Anom_{y, m} = \frac{zg_{y, m} - \bar{zg}_{m}}{\frac{1}{\sqrt{N}}\sum_y(zg_{y,m} - \bar{zg}_m)^2}
```

where $y$ indicates the time, and $m$ is the monthly index.
### AMOC
```bash
volmip-amoc CanESM5 r1i1p2f1 --experiment-id=volc-pinatubo-full --extend --cmip-folder path/to/cmip/data
```

Atlantic meridional overturning circulations is computed as the yearly averaged maximum of 
zonally integrated meridional stream function in the North Atlantic basin, between 20N to 60N.
Before annual average calculations values are deseasonalized and normalized by the standard 
deviation of that month (See NAO anomaly calculation).

## Tests

Tests are not distributed with the installed code, but can be run by

```bash
git clone git@gitlab.com:LandonRieger/volmip-date-selector.git
pip istall -e .
pytest tests
```